# README #

This sub-folder contains a DTSS subscription configuration file for the Reltio golden Life Sciences configuration. It contains the following file:

golden_tenant_DTSS_configuration.json

NPI DT - 

QA - https://test.reltio.com/ui/xu2AqfyTKxYDTuQ

Production - https://361.reltio.com/ui/snfnQfdwQLUpBSv

DEA DT -

QA - https://test.reltio.com/ui/SCPD70GezfJCsj3

Production - https://361.reltio.com/ui/7t88ntdQp08RxhT
