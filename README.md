LS Customer 360 is an application package, that has predefined Life Sciences Customer components.
The Life Sciences (LS) Customer 360 is a software solution to manage healthcare professionals, healthcare organizations and their affiliations by providing a single version of truth. The solution helps in centralizing and mastering the customer data, managing customer relationship effectively that result in better outcomes. The industry trusted data like NPI and DEA are available as Data-as-a-Service that can further enrich the customer data and for compliance purposes.

We designed it to be extensible, but still approachable using the default configuration. 



## Change Log

```
Last Update Date: 04/06/2018

Version: 2018.DDA.11
Description: DTSS configs are updated to contain only the attributes necessary
```

## Contributing
Please visit our [Conitributor Covenant Code of Conduct](https://bitbucket.org/reltio-ondemand/common/src/master/CodeOfConduct.md) to learn more about our contribution guidlines

## Licensing
```
Copyright (c) 2017 Reltio



Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at



    http://www.apache.org/licenses/LICENSE-2.0



Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

See the License for the specific language governing permissions and limitations under the License.
```

## Quick Start
To learn about LS Customer 360 data model view these [documents](https://bitbucket.org/reltio-ondemand/app-lscustomer360/src//Data%20Model/?at=master).


### Repository Owners

[Ramya Krishnan](ramya.krishnan@reltio.com)

[Yuriy Raskin](yuriy.raskin@reltio.com)


